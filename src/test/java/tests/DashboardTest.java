package tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Title;

import java.lang.reflect.Method;
import java.net.MalformedURLException;

/**
 * Created by vladyslav on 3/25/17.
 */
public class DashboardTest extends BaseTest {


    @BeforeMethod
    public void setUp(Method method)throws MalformedURLException {
        setUpDashboardTests(method);

    }

    @Title("Opening of the project page from dashboard ")
    @Test (priority = 1)
    public void openingProjectPageFromDashboard(){
        reporter.dashboard()
                .checkProjectName();
        reporter.dashboard()
                .clickViewReport()
                .verifyProjectPageOpen();
        reporter.dashboard()
                .verifyProjectIsCorrect();

    }


    @Title("Opening the page with devices list from dashboard")
    @Test (priority = 2)
    public void openingDevicesPageFromDashboard() {
        reporter.dashboard()
                .clickDevicesLink()
                .checkDevicePageIsOpen();
    }

    @Title("Log Out from the Reporter")
    @Test (priority = 2)
    public void logOutFromReporter(){
        reporter.dashboard()
                .logOut()
                .verifyLoginPageisShown();

    }



}
