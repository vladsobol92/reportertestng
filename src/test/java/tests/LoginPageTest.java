package tests;

import org.junit.Before;
import org.testng.annotations.*;
import org.testng.annotations.BeforeTest;
import ru.yandex.qatools.allure.annotations.Title;

import java.lang.reflect.Method;

import java.net.MalformedURLException;


/**
 * Created by vladyslav on 3/25/17.
 */
@Title("This is the test of Login functionality")
public class LoginPageTest extends BaseTest {


    @BeforeMethod
    public void setUp(Method method)throws MalformedURLException{
        setUpLoginPage(method);

    }


    @DataProvider (name = "credentials")
    public static Object [][] credentials(){
        return new Object[][] {{ "WrongUname", "12345678" },
                                { "SelfUser", "WRONG12345678" },
                                {"WrongName","WrongPass"}
        };
    }

    @Title("LogIn with valid credentials")
    @Test(priority = 1)
    public void loginWithCorrectCredentials(){
        reporter.loginPage().enterCredentials("SelfUser","12345678");
        reporter.loginPage().clickSubmit().verifyDashboardPageShown();

    }

    @Title("LogIn with invalid credentials")
    @Test(priority = 2, dataProvider = "credentials")
    public void loginWithIncorrectCredentials(String name, String pass){
        reporter.loginPage()
                .enterCredentials(name,pass);
        reporter.loginPage()
                .clickSubmit();
        reporter.loginPage()
                .verifyWarningMessage();

    }




}
