package tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Title;

import java.lang.reflect.Method;
import java.net.MalformedURLException;

/**
 * Created by vladyslav on 5/5/17.
 */
public class ScenarioPageTest extends BaseTest {



    @BeforeMethod
    public void setUp(Method method)throws MalformedURLException {

        setUpScenarioPageTests(method);


    }


    @Title("Switching between Steps and Devices tab on Scenario Page")
    @Test
    public void testStepsAndDevicesTabSwitch(){

        reporter.scenario().selectDevicesTab();
        reporter.scenario().expandTab();
        reporter.scenario().selectStepsTab();
        reporter.scenario().expandTab();
    }

    @Test
    public void playVideo(){
        reporter.scenario().waiting(4);
        reporter.scenario().checkTime();
        reporter.scenario().clickBtn("play");
        reporter.scenario().waiting(3);
        reporter.scenario().clickBtn("pause");
        reporter.scenario().compareTimes("different");

    }

    @Test
    public void pauseVideo(){
        reporter.scenario().waiting(4);
        reporter.scenario().clickBtn("play");
        reporter.scenario().waiting(3);
        reporter.scenario().clickBtn("pause");
        reporter.scenario().checkTime();
        reporter.scenario().waiting(3);
        reporter.scenario().compareTimes("equal");

    }

    @Test
    public void replayVideo(){
        reporter.scenario().waiting(4);
        reporter.scenario().checkTime();
        reporter.scenario().clickBtn("play");
        reporter.scenario().waiting(4);
        reporter.scenario().clickBtn("replay");
        reporter.scenario().compareTimes("equal");

    }




}


