package tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Title;

import java.lang.reflect.Method;
import java.net.MalformedURLException;

/**
 * Created by vladyslav on 5/12/17.
 */
public class GeneralTest extends BaseTest {

    @BeforeMethod
    public void setUp(Method method)throws MalformedURLException {
        setUpProjectPageTests(method);

    }

    @Title("Verification of the breadcrumbs on every page")
    @Test
    public void breadCrumbsPresentOnPages(){
            reporter.project().checkBreadCrumbs();
            reporter.project().clickViewLastSuite();
            reporter.suite().checkBreadCrumbs();
            reporter.suite().openScenario();
            reporter.scenario().checkBreadCrumbs();

    }

    @Test
    @Title("Functionality of the 'Back' button on each page")
    public void backButtonOnEachPage(){
        reporter.project().clickViewLastSuite();
        reporter.suite().openScenario();
        reporter.scenario().verifyScenarioPageIsOpened();
        reporter.scenario().clickBack();
        reporter.suite().verifySuitePageIsOpened();
        reporter.suite().clickBack();
        reporter.project().verifyProjectPageOpen();
        reporter.suite().clickBack();
        reporter.dashboard().verifyDashboardPageShown();
    }


}

