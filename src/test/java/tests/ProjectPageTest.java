package tests;

import org.junit.Before;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Title;

import java.lang.reflect.Method;
import java.net.MalformedURLException;

/**
 * Created by vladyslav on 4/14/17.
 */
@Title("Testing Project Page")
public class ProjectPageTest extends BaseTest {


    @BeforeMethod
    public void setUp(Method method)throws MalformedURLException {
        setUpProjectPageTests(method);

    }

    @Title("Switching between Tests and Devices button")
    @Test (priority = 2)
    public void testsAndDevicesButtonSwitch(){
        reporter.project().selectDevices();
        reporter.project().selectTests();

    }


    @Title("Selecting suites from dropdown on project page")
    @Test (priority = 2)
    public void sortSuitesWithDropdownList(){
        reporter.project().openSuitesDropdown();
        reporter.project().sortSuitesbyName();
    }

    @Title("Sort suites by date")
    @Test (priority = 2)
    public void sortSuitesWithDatePicker(){
        reporter.project().openDatePicker();
        reporter.project().setMonthYear("may","2017");
        reporter.project().setStartFinishDay("5","30");
        reporter.project().verifyProjectPageOpen();
        reporter.project().verifySuitesSortedByDate();

    }

    @Title("Cancell deleting of the suite")
    @Test (priority = 3)
    public void cancelDeletingSuite(){
        reporter.project().clickDeleteLastSuite();
        reporter.project().cancelDeleteLastSuite();
        reporter.project().verifyProjectPageOpen();

    }


    @DataProvider (name="suites")
    public static Object[][] state (){
        return new Object[][]{
                {"passed"},
                {"failed"},
                {"pending"},
        };

    }


    //@Title("Deleting the suite")
    //@Test (priority = 2,dataProvider = "suites")
    public void deleteSuite(String state){

        reporter.project().getSuitesAmount(state);
        reporter.project().expandTest(state);
        reporter.project().deleteSuite(state);
        reporter.project().confirmDeleteSuite();
        reporter.project().checkIfSuiteDeleted(state);

    }

    @Title("Expanding of the tabs with suite information")
    @Test (priority = 2)
    public void expandingSuiteTab(){
        reporter.project().getHeightOfSuiteTab();
        reporter.project().expandTab();

    }

    @Title("Opening the Suite Page")
    @Test(priority = 1)
    public void openingSuitePage(){
        reporter.project().clickViewLastSuite().
                verifySuitePageIsOpened();
    }


}
