package tests;


import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Title;

import java.lang.reflect.Method;
import java.net.MalformedURLException;

/**
 * Created by vladyslav on 3/25/17.
 */
public class ForgotPasswordTest extends BaseTest {


    @BeforeMethod
    public void setUp(Method method) throws MalformedURLException{
        setUpLoginPage(method);
        reporter.loginPage().openForm();


    }

    @DataProvider(name = "uNames")
    public static Object [][] uName(){
        return new Object[][] {
                { "SelfUser" },
                { "adidas1" },
        };
    }

    @Title("Forgot password request for users from different groups")
    @Test(priority = 1, dataProvider = "uNames")
    public void ForgotPasswordRequestCorrectUname (String uName) {
        reporter.forgotPass().enterUsername(uName);
        reporter.forgotPass().getUsernameValue();
        reporter.forgotPass().clickSubmit();
        reporter.forgotPass().checkSuccessMessage();
        reporter.forgotPass().checkFormShownCorrectly();


    }

    @Title("Forgot Password request for NON existing username ")
    @Test (priority = 2)
    public void ForgotPasswordRequestWrongUname(){
        reporter.forgotPass().enterUsername("Test");
        reporter.forgotPass().clickSubmit();
        reporter.forgotPass().checkNegativeMessage();

    }

    @Title("Cancelling of the Forgot Password request")
    @Test(priority = 3)

    public void CancelForgotPasswordRequest(){
        reporter.forgotPass()
                .enterUsername("Test");
        reporter.forgotPass().cancel()
                .verifyLoginPageisShown();
    }





}
