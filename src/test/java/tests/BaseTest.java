package tests;

import helpers.TestRecorders;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import facade.Reporter;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.properties.annotations.Resource;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;


/**
 * Created by vladyslav on 4/14/17.
 */
public class BaseTest extends TestRecorders {


    WebDriver driver;
    Reporter reporter;


    public void setDriver() throws MalformedURLException{
       // System.setProperty("webdriver.chrome.driver",
                //"/Users/vladyslav/IdeaProjects/seleniumproject/src/main/resources/webdriver/chromedriver");
        driver = new ChromeDriver();
        reporter=new Reporter(driver);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        startRecording(driver);
    }

    public void openReporter(){
        driver.get("http://localhost:3000/");
    }


    private void loginToReporter(){
        reporter.loginPage().enterCredentials("SelfUser","12345678");
        reporter.loginPage().clickSubmit();

    }

    public void setUpLoginPage (Method method) throws MalformedURLException{
        setDriver();
        openReporter();
        startLog(method);


    }

    public void openProjectPage(){
            reporter.dashboard().clickViewReport();

    }

    public void setUpDashboardTests(Method method)throws MalformedURLException{
        setUpLoginPage(method);
        loginToReporter();
    }

    public void setUpProjectPageTests(Method method)throws MalformedURLException{
        setUpLoginPage(method);
        loginToReporter();
        openProjectPage();
    }

    public void setUpSuitePageTests(Method method)throws MalformedURLException{
        setUpProjectPageTests(method);
        reporter.project().clickViewLastSuite();
    }

    public void setUpScenarioPageTests(Method method)throws MalformedURLException{
        setUpSuitePageTests(method);
        reporter.suite().openScenario();

    }



    @AfterMethod
    public void tearDown(ITestResult result, Method meth) {
        if (driver != null) {
            
            stopLog(result);
            stopRecording(meth.getName(),result);
            driver.quit();
        }
    }

}
