package tests;


import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Title;


import java.lang.reflect.Method;
import java.net.MalformedURLException;

/**
 * Created by vladyslav on 4/30/17.
 */
public class SuitePageTest extends BaseTest {


    @BeforeMethod
    public void setUp(Method method)throws MalformedURLException {
        setUpSuitePageTests(method);

    }

    @Title("Switching between tests and devices button on Suite page")
    @Test (priority = 2)
    public void testsAndDevicesButtonSwitch(){
        reporter.suite().selectDevices();
        reporter.suite().selectTests();
        reporter.suite().selectPassedTests();
        reporter.suite().expandTab();
        reporter.suite().selectFailedTests();
        reporter.suite().expandTab();
    }

    @Test (priority = 1)
    @Title("Opening Scenario page")
    public void openScenarioPage(){
        reporter.suite().selectPassedTests();
        reporter.suite().expandTab();
        reporter.suite().clickScenarioLink()
                .verifyScenarioPageIsOpened();


    }




}
