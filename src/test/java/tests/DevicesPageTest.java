package tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Title;


import java.lang.reflect.Method;
import java.net.MalformedURLException;

/**
 * Created by vladyslav on 4/26/17.
 */
public class DevicesPageTest extends BaseTest {

    @BeforeMethod
    public void setUp(Method method) throws MalformedURLException {
        setUpDashboardTests(method);

    }

    @Title("Checking the list of the devices and expanding of the devices tab ")
    @Test
    public void devicesTabsShownAndExpandable(){
        reporter.dashboard().clickDevicesLink();
        reporter.devices().checkDevicePageIsOpen();
        reporter.devices().showDevices("free");
        reporter.devices().showDevices("busy");
        reporter.devices().showDevices("invalid");
        //Rewrite to expand tabs with all states
        reporter.devices().expandTab();

    }


}
