package pages;


import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by vladyslav on 4/19/17.
 */
public class DevicesPage extends BasePage {

    Logger Log = Logger.getLogger(DevicesPage.class.getName());

    By title=By.className("title");


    public DevicesPage (WebDriver driver){
        super(driver);

    }

    public void checkDevicePageIsOpen(){
        //System.out.println("Verifying that devices page is open...");
        Log.info("Verifying that devices page is open...");
        wait.until(ExpectedConditions.visibilityOfElementLocated(title)).isDisplayed();
        Assert.assertEquals("Connected devices", driver.findElement(title).getText());
        Assert.assertTrue(driver.getCurrentUrl().contains("devices"));
    }


    public WebElement[] getDevices(By by){
        ProjectPage proj=new ProjectPage(driver);
       return proj.getSuites(by);
    }

    public void showDevices(String state){
        By locator;
        switch (state.toLowerCase()){
            case "free":
                locator=greenItem;
                break;
            case "busy":
                locator=yellowItem;
                break;
            case "invalid":
                locator=redItem;
                break;
           default:
                locator=greenItem;
        }
        try {Assert.assertTrue(getDevices(locator).length!=0);}
        catch (AssertionError e){
            //System.out.println("No devices with state: "+ state);
            Log.info("No devices with state: "+ state);
        }
        for ( WebElement device:getDevices(locator)) {
            //System.out.println(state +" Device: " +device.getText());
            Log.info(state +" Device: " +device.getText());
        }
    }





}
