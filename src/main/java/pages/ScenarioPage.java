package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by vladyslav on 5/5/17.
 */
public class ScenarioPage extends BasePage {

    static int timeStart;

    By stepsTab = By.xpath("//*[@role='tab' and text()='STEPS']/..");
    By devicesTab = By.xpath("//*[@role='tab' and text()='DEVICES']/..");
    By play = By.xpath(".//playback-controls//button[1]");
    By pause = By.xpath(".//playback-controls//button[2]");
    By replay = By.xpath(".//playback-controls//button[3]");

    public ScenarioPage(WebDriver driver){
        super(driver);
    }

    public void checkBreadCrumbs(){
        Log.info("Checking Breadcrumbs...");
        int x = getBreadCrumbs();
        //System.out.println("Checking breadcrumbs...");
        Log.info("Checking breadcrumbs...");
        Assert.assertTrue(x==4);
    }


    public void verifyScenarioPageIsOpened(){
        //System.out.println("Verifying scenario page is displayed...");
        Log.info("Verifying scenario page is displayed...");
        wait.until(ExpectedConditions.visibilityOfElementLocated(infoBlock));
        Assert.assertTrue(driver.getCurrentUrl().contains("scenario"));
        Assert.assertTrue("Screenshot player is not shown !",
                driver.findElement(infoBlock).isDisplayed());
    }

    public void selectStepsTab() {
        //System.out.println("Clicking Steps tab");
        Log.info("Clicking Steps tab...");
        driver.findElement(stepsTab).click();
        Assert.assertEquals("active",driver.findElement(stepsTab).getAttribute("class"));
    }

    public void selectDevicesTab() {
        //System.out.println("Clicking Devices tab");
        Log.info("Clicking Devices tab...");
        driver.findElement(devicesTab).click();
        Assert.assertEquals("active",driver.findElement(devicesTab).getAttribute("class"));
    }

    public String getTime(){

        String time = driver.findElement(By.xpath(".//clock/span[1]")).getText();
        return time;
    }

    public void checkTime(){
        String [] time=getTime().split(":");
        timeStart=Integer.parseInt(time[1]);
    }

    public void clickBtn (String btn){
        if (btn=="play"){
            driver.findElement(play).click();
        }
        else if (btn=="pause"){
            driver.findElement(pause).click();
        }
        else if (btn=="replay"){
            driver.findElement(replay).click();
        }
        else {
            System.out.println("No such button in the player ! "+ btn);
        }
    }

    public void waiting (int seconds){

        int mills = seconds * 1000;
        try {Thread.sleep(mills);}
        catch (Exception ex){}
    }

    public void compareTimes (String condition){
        String [] time= getTime().split(":");
        int seconds=Integer.parseInt(time[1]);
        if (condition=="different"){Assert.assertTrue(timeStart < seconds);}
        else if (condition=="equal"){Assert.assertTrue(timeStart == seconds);}
        System.out.println("This is startTime: "+timeStart );
        System.out.println("This is time after several seconds: "+seconds);
    }

}
