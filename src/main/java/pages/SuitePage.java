package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by vladyslav on 4/30/17.
 */
public class SuitePage extends BasePage {


    By FailedTestsBtn = By.xpath("//*[@role='tab' and text()='Failed tests']/..");
    By PassedTestsBtn = By.xpath("//*[@role='tab' and text()='Passed tests']/..");
    By scenarioLink=By.xpath(".//*[@class='col-sm-8']");




    public SuitePage(WebDriver driver) {
        super(driver);
    }

    public void verifySuitePageIsOpened(){
        //System.out.println("Verifying suite page is displayed...");
        Log.info("Verifying suite page is displayed...");
        wait.until(ExpectedConditions.visibilityOfElementLocated(infoBlock));
        Assert.assertTrue(driver.getCurrentUrl().contains("suite"));
        Assert.assertTrue("Latest run block is not shown !",
                driver.findElement(infoBlock).isDisplayed());
    }

    public void checkBreadCrumbs(){
        int x = getBreadCrumbs();
        //System.out.println("Checking breadcrumbs...");
        Log.info("Checking breadcrumbs...");
        Assert.assertTrue(x==3);
    }


    public void selectFailedTests() {
        //System.out.println("Clicking Failed Tests tab");
        Log.info("Clicking Failed Tests tab...");
        driver.findElement(FailedTestsBtn).click();
        Assert.assertEquals("active",driver.findElement(FailedTestsBtn).getAttribute("class"));
    }

    public void selectPassedTests() {
        //System.out.println("Clicking Passed Tests tab");
        Log.info("Clicking Passed Tests tab...");
        driver.findElement(PassedTestsBtn).click();
        Assert.assertEquals("active",driver.findElement(PassedTestsBtn).getAttribute("class"));
    }

    public ScenarioPage clickScenarioLink(){
            Log.info("Clicking scenario link...");
            WebElement scenario=driver.findElement(scenarioLink);
            scenario.click();

        return new ScenarioPage(driver);
    }

    public ScenarioPage openScenario(){
        //System.out.println("Opening scenario...");
        Log.info("Opening scenario...");
        selectPassedTests();
        try {
            expandTab();
            WebElement scenario=driver.findElement(scenarioLink);
            scenario.click();

        }catch (NoSuchElementException ex){
            selectFailedTests();
            expandTab();
            WebElement scenario=driver.findElement(scenarioLink);
            scenario.click();
        }
        return new ScenarioPage(driver);
    }



}


