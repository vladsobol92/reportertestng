package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by vladyslav on 4/14/17.
 */
public class ProjectPage extends BasePage {

static int suiteAmount;


    By viewReportLastSuite = By.xpath(".//a[text()='View full report']");
    By deleteLastSuite = By.xpath(".//button[1][text()='Delete suite']");
    By deleteFailedSuite=By.xpath(".//*[@class='report-item danger']//button[text()='Delete suite']");
    By deletePassedSuite=By.xpath(".//*[@class='report-item success']//button[text()='Delete suite']");
    By deleteUnfinishedSuite=By.xpath(".//*[@class='report-item warning']//button[text()='Delete suite']");
    By dialogWindow = By.xpath(".//md-dialog-container");
    By cancelButton = By.xpath(".//span[text()='Cancel']/..");
    By confirmButton = By.xpath(".//span[text()='Confirm']/..");
    By dropDownWithSuites = By.xpath(".//button[@class='dropdown-toggle']");
    By suitesInDropdown = By.xpath("html/body/my-app/header-bar/ng-component/div/div[2]/div/ul/li[3]/ul/li/a");
    By datePickerBtn= By.xpath("//*[@class='selbtngroup']");
    By calendar=By.cssSelector(".selector");

    public ProjectPage(WebDriver driver) {
        super(driver);
    }



    public void verifyProjectPageOpen() {
        //System.out.println("Verifying project page is displayed...");
        Log.info("Verifying project page is displayed...");
        wait.until(ExpectedConditions.visibilityOfElementLocated(infoBlock));
        Assert.assertTrue("URL of opened page is incorrect",
                driver.getCurrentUrl().contains("projects"));
        Assert.assertTrue("Last suite block is not shown !",
                driver.findElement(infoBlock).isDisplayed());

    }

    public String getProjectUrl(){
        //System.out.println("Getting url of current project...");
        Log.info("Getting url of current project...");
        String url=driver.getCurrentUrl();
        return url;
    }


    public void checkBreadCrumbs(){
        int x = getBreadCrumbs();
        //System.out.println("Checking breadcrumbs...");
        Log.info("Checking breadcrumbs...");
        Assert.assertTrue(x==2);
    }

    public void clickDeleteLastSuite(){
        //System.out.println("User is clicking Delete button on Last Suite...");
        Log.info("User is clicking Delete button on Last Suite...");
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(deleteLastSuite))
                    .click();
            wait.until(ExpectedConditions.presenceOfElementLocated(dialogWindow));
        }
       catch (Exception e){
           //System.out.println("Suite in progress or user has no rights to delete !");
           Log.info("Suite in progress or user has no rights to delete !");
       }

    }

    public void cancelDeleteLastSuite(){
        try{
            //System.out.println("User is clicking Cancel button in dialog window...");
            Log.info("User is clicking Cancel button in dialog window...");
            driver.findElement(cancelButton).click();
        }
        catch (NoSuchElementException ex){

        }
        List <WebElement> list=driver.findElements(dialogWindow);
        Assert.assertTrue("Dialog box is still visible",list.size()==0);

    }

    public void confirmDeleteSuite(){
        //System.out.println("User is clicking Confirm button in dialog window...");
        Log.info("User is clicking Confirm button in dialog window...");
        driver.findElement(confirmButton).click();
        WebDriverWait w=new WebDriverWait(driver,15);
        w.until(ExpectedConditions.visibilityOfElementLocated(viewReportLastSuite));

    }


    public SuitePage clickViewLastSuite(){
        //System.out.println("User clicks view last suite...");
        Log.info("User clicks view last suite...");
        driver.findElement(viewReportLastSuite).click();
        return new SuitePage(driver);
    }

    public WebElement[] getSuites(By by){
        List <WebElement> suites=driver.findElements(by);
        WebElement elements []= new WebElement[suites.size()];
        suites.toArray(elements);
        return elements;
    }

    public int getSuitesAmount(String state){
        By locator;
        int suites=0;
        switch (state.toLowerCase()){
            case "passed":
                locator=greenItem;
                break;
            case "failed":
                locator=redItem;
                break;
            case "pending":
                locator=yellowItem;
                break;
            default:
                locator=greenItem;
        }
        try {Assert.assertTrue(getSuites(locator).length!=0);
        suites = getSuites(locator).length;}
        catch (AssertionError e){
            //System.out.println("No suites with state: "+ state);
            Log.info("No suites with state: "+ state);
        }
        suiteAmount=suites;
        return suites;
    }

    public void checkIfSuiteDeleted(String state){
        By locator;
        switch (state.toLowerCase()){
            case "passed":
                locator=greenItem;
                break;
            case "failed":
                locator=redItem;
                break;
            case "pending":
                locator=yellowItem;
                break;
            default:
                locator=greenItem;
        }
        int suites=getSuites(locator).length;

        Assert.assertTrue(suiteAmount>suites);

    }


    public void expandTest(String testState){
        if (testState =="passed"){
            WebElement failed[]=getSuites(greenItem);
            failed[0].click();
        }
        else if (testState =="failed") {
            WebElement failed[] = getSuites(redItem);
            failed[0].click();
        }

        else if (testState =="pending") {
            WebElement failed[] = getSuites(yellowItem);
            failed[0].click();
        }
    }


    public WebElement findDeleteInSuiteTab(By by){
        return driver.findElement(by);
    }

    public void deleteSuite(String testState){
        if (testState =="passed") {
            wait.until(ExpectedConditions.visibilityOfElementLocated(deletePassedSuite));
            WebElement delete = findDeleteInSuiteTab(deletePassedSuite);
            delete.click();
        }
       else if (testState =="failed") {
            wait.until(ExpectedConditions.visibilityOfElementLocated(deleteFailedSuite));
            WebElement delete = findDeleteInSuiteTab(deleteFailedSuite);
            delete.click();
        }
       else if (testState =="pending") {
            wait.until(ExpectedConditions.visibilityOfElementLocated(deleteUnfinishedSuite));
            WebElement delete = findDeleteInSuiteTab(deleteUnfinishedSuite);
            delete.click();
        }

        //System.out.println("Deleting " + testState + " suite");
        Log.info("Deleting " + testState + " suite");
    }

    public void openSuitesDropdown(){
        driver.findElement(dropDownWithSuites).click();


    }
    public WebElement[] getSuitesFromDropdown() {
        List<WebElement> names = driver.findElements(suitesInDropdown);
        WebElement suites[] = new WebElement[names.size()];
        names.toArray(suites);
        return suites;
    }


    public void sortSuitesbyName(){
        int x=1;
        int y=1;
       WebElement suite [] = getSuitesFromDropdown();
        //System.out.println("Suites in list:");
        Log.info("Suites in list:");
       while (y < suite.length && y<3){
           //System.out.println(suite[y].getText());
           Log.info(suite[y].getText());
           y++;
       }
        //System.out.println("And "+ (suite.length - y) + " more suites");
        Log.info("And "+ (suite.length - y) + " more suites");

       while (x < suite.length && x<3){
           String name=suite[x].getText();
           //System.out.println("Current suite: " + name);
           Log.info("Current suite: " + name);
           suite[x].click();
           verifyProjectPageOpen();
           String lastSuite=driver.findElement(By.xpath(".//h1")).getText();
           //System.out.println("LastRun: " + lastSuite);
           Log.info("LastRun: " + lastSuite);
           driver.findElement(dropDownWithSuites).click();
           List<WebElement> names = driver.findElements(suitesInDropdown);
           names.toArray(suite);
           x=x+1;
           Assert.assertEquals("Names not match",name,lastSuite);
        }

    }

    public void openDatePicker(){
        //System.out.println("Opening dayPicker...");
        Log.info("Opening dayPicker...");
        WebElement btn = driver.findElement(datePickerBtn);
        btn.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(calendar));
    }

    public void setMonthYear(String mon, String yr){

        //System.out.println("Setting a date...");
        Log.info("Setting a date..." + mon + " " + yr);
        driver.findElement(By.cssSelector(".monthlabel")).click();
        WebElement month=driver.findElement(By.cssSelector(".monthinput"));
        month.clear();
        month.sendKeys(mon);
        driver.findElement(By.cssSelector(".yearlabel")).click();
        WebElement year=driver.findElement(By.cssSelector(".yearinput"));
        year.clear();
        year.sendKeys(yr);

    }

    public void setStartFinishDay (String start, String finish) {
        Log.info("Setting start and finish days" + start + " " + finish);
        List <WebElement> days=driver.findElements(By.cssSelector(".currmonth>span"));
        for (WebElement day:days){
           String text = day.getText();
            if(text.equals(start)){
                    day.click();
                    break;
            }
        }
        List <WebElement> days2=driver.findElements(By.cssSelector(".currmonth>span"));
        for (WebElement day2:days2){
            String text = day2.getText();
            if(text.equals(finish)){
                day2.click();
                break;
            }
        }
        driver.findElement(By.cssSelector(".footerbtn")).click();
    }

    public void verifySuitesSortedByDate(){
            Log.info("Verifying that suites are sorted by date...");
            String url = driver.getCurrentUrl();
            Assert.assertTrue(url.contains("startDate=") && url.contains("endDate="));
    }



}
