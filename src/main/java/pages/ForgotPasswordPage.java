package pages;


import helpers.TestRecorders;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by vladyslav on 3/24/17.
 */
public class ForgotPasswordPage extends TestRecorders{
    WebDriver driver;
    WebDriverWait wait;
   static String uName;



    By username = By.name("username");
    By submitButton = By.xpath("*//button");
    By cancelButton = By.cssSelector(".btn.btn-default");
    By message=By.className("text-danger");
    By successMessage= By.className("text-success");
    By newPassword=By.id("newPassword");
    By confirmNewPass= By.id("confPassword");
    By verifCode= By.id("verifCode");


    public ForgotPasswordPage(WebDriver driver) {
        this.driver = driver;
        wait=new WebDriverWait(driver, 4);

    }


    public WebElement getUsernameField(){
        return driver.findElement(username);

    }

    public String getUsernameValue(){
        String name=getUsernameField().getAttribute("ng-reflect-model");
        uName=name;
        return uName;
    }

    public void setUsername(String strUsername) {
        driver.findElement(username).clear();
        driver.findElement(username).sendKeys(strUsername);

    }



    public void enterUsername(String strUsername) {
        //System.out.println("User Enters username: " + strUsername);
        Log.info("User Enters username: " + strUsername);
        this.setUsername(strUsername);

    }

    public void clickSubmit(){
        driver.findElement(submitButton).click();
    }

    public LoginPage cancel() {
        //System.out.println("Clicking Cancell button...");
        Log.info("Clicking Cancell button...");
        driver.findElement(cancelButton).click();
        return new LoginPage(driver);
    }


    public void checkNegativeMessage(){

        //System.out.println("Checking negative message is shown...");
        Log.info("Checking negative message is shown...");
        wait.until(ExpectedConditions.presenceOfElementLocated(message));
        Assert.assertEquals("Authentication failed",wait.until(ExpectedConditions.presenceOfElementLocated(message)).getText());


    }

    public void checkSuccessMessage(){

        //System.out.println("Checking Success message is shown...");
        Log.info("Checking Success message is shown...");

        wait.until(ExpectedConditions.presenceOfElementLocated(successMessage));

    }



    public WebElement getVerifCodeField(){

        //System.out.println("Checking Verification Code field is shown...");
        Log.info("Checking Verification Code field is shown...");
         return driver.findElement(verifCode);

    }

    public WebElement getNewPasswordField(){

        //System.out.println("Checking New Password field is shown...");
        Log.info("Checking New Password field is shown...");
        return driver.findElement(newPassword);

    }

    public WebElement getConfirmNewPasswordField(){

        //System.out.println("Checking Confirm New Password field is shown...");
        Log.info("Checking Confirm New Password field is shown...");
        return driver.findElement(confirmNewPass);

    }


    public void checkFormShownCorrectly(){
        String text = getUsernameField().getAttribute("ng-reflect-model");
        Assert.assertEquals(uName,text);
        Assert.assertTrue(getVerifCodeField().isDisplayed());
        Assert.assertTrue(getNewPasswordField().isDisplayed());
        Assert.assertTrue(getConfirmNewPasswordField().isDisplayed());
        Assert.assertTrue(getUsernameField().isDisplayed());


    }


}

