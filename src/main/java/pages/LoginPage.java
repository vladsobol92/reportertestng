package pages;


import helpers.TestRecorders;
import org.apache.log4j.Level;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;


/**
 * Created by vladyslav on 3/23/17.
 */
public class LoginPage extends TestRecorders {

   WebDriver driver;
   WebDriverWait wait;




    By loginField=By.id("username");
    By passwordField = By.id("password");
    By submittButton= By.tagName("button");
    By message=By.xpath("//label[text()='Authentication failed']");
    By link= By.cssSelector(".forgot-pass");




    public LoginPage (WebDriver driver){
        this.driver=driver;
        wait=new WebDriverWait(driver, 4);


    }




    public void verifyLoginPageisShown(){
        //System.out.println("Verifying LogIn page is shown...");
        Log.info("Verifying LogIn page is shown...");
        wait.until(ExpectedConditions.presenceOfElementLocated(loginField)).isDisplayed();
        wait.until(ExpectedConditions.presenceOfElementLocated(passwordField)).isDisplayed();



    }

    public void enterUsername(String strUsername){

        driver.findElement(loginField).sendKeys(strUsername);

    }

    public void enterPassword(String strPassword){

        driver.findElement(passwordField).sendKeys(strPassword);

    }

    public void enterCredentials(String strUsername, String strPassword){
        //System.out.println("User is trying to login with given credentials: " + strUsername +" "+ strPassword);
        Log.info("User is trying to login with given credentials: " + strUsername +" "+ strPassword);
        this.enterUsername(strUsername);
        this.enterPassword(strPassword);


    }

    public DashboardPage clickSubmit(){
        //System.out.println("User clicks sign in button");
        Log.info("User clicks sign in button");
        driver.findElement(submittButton).click();
        return new DashboardPage(driver);

    }


    public void verifyWarningMessage(){
        //System.out.println("Checking that warning message is shown...");
        Log.info("Checking that warning message is shown...");
        wait.until(ExpectedConditions.presenceOfElementLocated(message)).isDisplayed();

    }

    public ForgotPasswordPage openForm(){
        //System.out.println("Opening Forgot Password form...");
        Log.info("Opening Forgot Password form...");
        driver.findElement(link).click();

        return new ForgotPasswordPage(driver);

    }





}
