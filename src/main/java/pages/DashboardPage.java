package pages;

import helpers.TestRecorders;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by vladyslav on 3/23/17.
 */
public class DashboardPage extends TestRecorders {
    static WebDriver driver;
    static WebDriverWait  wait;
    static String projname;



    By user=By.className("person");
    By dropMenu =By.className("dropdown-toggle");
    By devicesLink = By.xpath(".//*[@id='navbar']//a[text()='Devices']");
    By logOutButton=By.xpath(".//*[@id='navbar']//a[text()='Log out']");
    By viewReportBtn=By.xpath("//li[div[h2[text()='CPTA_Testing_Vlad']]]//a[text()='View full report']");
    By storageBar=By.xpath("//div[@class='progress']");
    By project=By.xpath("//ul[@class='projects']/li/div[1]/h2");
    By projectname=By.xpath("//li[div[h2[text()='CPTA_Testing_Vlad']]]/div/*[contains(.,'CPTA')]");



    public DashboardPage (WebDriver driver){
        this.driver=driver;
        this.wait=new WebDriverWait(driver,6);

    }


    public void verifyDashboardPageShown(){
        //System.out.println("Verify dashboard page is shown...");
        Log.info("Verify dashboard page is shown...");
        wait.until(ExpectedConditions.presenceOfElementLocated(user)).isDisplayed();
        wait.until(ExpectedConditions.presenceOfElementLocated(storageBar)).isDisplayed();
        Assert.assertTrue(driver.getCurrentUrl().contains("reporter"));
        showProjects();


    }

    public WebElement[] getProjects(){
       List <WebElement> list=driver.findElements(project);
       WebElement projects[]=new WebElement [list.size()];
       list.toArray(projects);
       return projects;
    }

    public void showProjects() {
        if(getProjects().length<1){
            //System.out.println("No projects for current company");
            Log.info("No projects for current company");
        }
        else {
            //System.out.println("User's projects:");
            Log.info("User's projects:");
            for (WebElement name : getProjects()) {
                //System.out.println(name.getText());
                Log.info(name.getText());
            }
        }
    }

    public ProjectPage clickViewReport(){
        //System.out.println("User opens CPTA_Testing_Vlad project...");
        Log.info("User opens CPTA_Testing_Vlad project...");
        driver.findElement(viewReportBtn).click();
        return  new ProjectPage(driver);

    }


    public DevicesPage clickDevicesLink(){
        //System.out.println("User opens drop menu...");
        Log.info("User opens drop menu...");
        wait.until(ExpectedConditions.presenceOfElementLocated(dropMenu))
                .click();
        //System.out.println("User cliks devices button...");
        Log.info("User cliks devices button...");
        wait.until(ExpectedConditions.presenceOfElementLocated(devicesLink))
        .click();
        return new DevicesPage(driver);

    }



    public void checkProjectName(){
        //System.out.println("Checking project name...");
        Log.info("Checking project name...");
        projname=driver.findElement(projectname).getText();
        //System.out.println("Project: "+projname);
        Log.info("Project: "+projname);


    }

    public void verifyProjectIsCorrect(){
        //System.out.println("Verifying correct project is open...");
        Log.info("Verifying correct project is open...");
        ProjectPage project= new ProjectPage(driver);
        String projUrl=project.getProjectUrl();
        Assert.assertTrue(projUrl.contains(projname));
        //System.out.println(projUrl);
        Log.info(projUrl);

    }


    public LoginPage logOut(){
        //System.out.println("User is clicking drop down list");
        Log.info("User is clicking drop down list");
        driver.findElement(dropMenu).click();
        //System.out.println("User is clicking LogOut button");
        Log.info("User is clicking drop down list");
        driver.findElement(logOutButton).click();
        return new LoginPage(driver);
    }


}
