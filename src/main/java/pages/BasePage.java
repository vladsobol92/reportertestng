package pages;

import helpers.TestRecorders;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by vladyslav on 5/4/17.
 */
public class BasePage extends TestRecorders {

     WebDriver driver;
     WebDriverWait wait;



    By backBtn=By.xpath(".//a[@class='btn-back']");
    By breadcrumb= By.xpath(".//breadcrumb/ul//li");
    By DevicesBtn = By.xpath("//*[@role='tab' and text()='Devices']/..");
    By TestsBtn = By.xpath("//*[@role='tab' and text()='Tests']/..");
    By infoBlock = By.xpath(".//div[@class='overview']");

    By tab = By.cssSelector(".report-item");
    By greenItem = By.xpath(".//*[@class='report-item success']");
    By redItem = By.xpath(".//*[@class='report-item danger']");
    By yellowItem = By.xpath(".//*[@class='report-item warning']");


    public BasePage (WebDriver driver){
        this.driver=driver;
        wait = new WebDriverWait(driver, 8);
    }


    public void selectDevices() {
        //System.out.println("Clicking Devices button");
        Log.info("Clicking Devices button");
        driver.findElement(DevicesBtn).click();
        Assert.assertEquals("active", driver.findElement(DevicesBtn).getAttribute("class"));
    }

    public void selectTests() {
        //System.out.println("Clicking Tests button");
        Log.info("Clicking Tests button");
        driver.findElement(TestsBtn).click();
        Assert.assertEquals("active", driver.findElement(TestsBtn).getAttribute("class"));

    }

    public void clickBack(){
        //System.out.println("Clicking back button...");
        Log.info("Clicking back button...");
        driver.findElement(backBtn).click();
    }

    public int getHeightOfSuiteTab(){

        WebElement tab = driver.findElement(this.tab);
        int x = tab.getSize().getHeight();
        return x;

    }

    public int getBreadCrumbs(){
        List <WebElement> crumbs=driver.findElements(breadcrumb);
        int x =crumbs.size();
        return x;
    }



    public void expandTab(){

        try{
            //System.out.println("Expanding tab ...");
            Log.info("Expanding tab ...");
            int notExpanded=getHeightOfSuiteTab();

            driver.findElement(this.tab).click();
                try {
                    Thread.sleep(2000);
                }
                catch (InterruptedException e){
                }
            //System.out.println("Comparing height of tab ...");
            Log.info("Comparing height of tab ...");
            int expanded=getHeightOfSuiteTab();
            Assert.assertTrue((notExpanded*2)<expanded);}
        catch (Exception ex){
            //System.out.println("No tabs to expand");
            Log.info("No tabs to expand");
        }



    }

}


