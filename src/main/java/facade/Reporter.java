package facade;

import org.openqa.selenium.WebDriver;
import pages.*;


/**
 * Created by vladyslav on 4/23/17.
 */
public class Reporter {
    WebDriver driver;

    public Reporter(WebDriver driver){
        this.driver=driver;
    }

    public LoginPage loginPage (){
        return new LoginPage(driver);
    }

    public ForgotPasswordPage forgotPass (){
        return new ForgotPasswordPage(driver);
    }

    public DashboardPage dashboard(){
        return new DashboardPage(driver);
    }

    public ProjectPage project(){
        return new ProjectPage(driver);
    }

    public SuitePage suite(){
        return new SuitePage(driver);

    }

    public DevicesPage devices(){
        return new DevicesPage(driver);
    }

    public ScenarioPage scenario() {return new ScenarioPage(driver);}

}
