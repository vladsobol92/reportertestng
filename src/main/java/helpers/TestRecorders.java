package helpers;

import org.apache.log4j.Logger;
import org.testng.ITestResult;
import org.monte.media.Format;
import org.monte.media.FormatKeys;
import org.monte.media.math.Rational;
import org.monte.screenrecorder.ScreenRecorder;
import org.openqa.selenium.*;

import java.awt.*;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.monte.media.FormatKeys.*;
import static org.monte.media.FormatKeys.EncodingKey;
import static org.monte.media.FormatKeys.FrameRateKey;
import static org.monte.media.VideoFormatKeys.*;

/**
 * Created by vladyslav on 5/29/17.
 */
public class TestRecorders {

    //This is a part for recording Logs of test progress
    //
    //

   public Logger Log = Logger.getLogger(TestRecorders.class.getName());

    public void startLog(Method method){
        String testName=method.getName();
        Log.info("+++++++++++++++++++ STARTING TEST" +testName + "++++++++++++++++++++++++");

    }

    public void stopLog (ITestResult result){

        if (result.getStatus() == ITestResult.SUCCESS) {
            Log.info("**********  PASSED **********");
        } else if (result.getStatus() == ITestResult.FAILURE) {
            Log.error("************ FAILURE ************");

        } else if (result.getStatus() == ITestResult.SKIP) {

            Log.info("*********** SKIPPED ***********");
        }
        Log.info("+++++++++++++++++++ TEST FINISHED +++++++++++++++++++++++");

    }




    //This is a part for recording Videos
    //
    //
    //

    private final String RECORD_DIRECTORY = "/Users/vladyslav/IdeaProjects/reporterTestNg/src/video/";

    private ScreenRecorder screenRecorder;

    public void startRecording(WebDriver driver) {

        try {
            GraphicsConfiguration gc = GraphicsEnvironment
                    .getLocalGraphicsEnvironment().getDefaultScreenDevice()
                    .getDefaultConfiguration();

            File dir = new File(RECORD_DIRECTORY);

            // Record only browsers window
            // to decrease the size of the file
            org.openqa.selenium.Point point = driver.manage().window().getPosition();
            org.openqa.selenium.Dimension dimension = driver.manage().window().getSize();

            Rectangle rectangle = new Rectangle(point.x, point.y,
                    dimension.width, dimension.height);

            this.screenRecorder = new ScreenRecorder(gc, rectangle,
                    new Format(MediaTypeKey, FormatKeys.MediaType.FILE, MimeTypeKey,
                            MIME_AVI),
                    new Format(MediaTypeKey, FormatKeys.MediaType.VIDEO, EncodingKey,
                            ENCODING_AVI_MJPG,
                            CompressorNameKey,
                            ENCODING_AVI_MJPG, DepthKey,
                            24, FrameRateKey, Rational.valueOf(12), QualityKey,
                            1.0f, KeyFrameIntervalKey, 15 * 60), new Format(
                    MediaTypeKey, MediaType.VIDEO, EncodingKey,
                    "black", FrameRateKey, Rational.valueOf(12)), null,
                    dir);

            this.screenRecorder.start();

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void stopRecording(String recordName, ITestResult result) {

        //Rename newly recorded video
        //We save video only for the tests with FAILURE.
        //We record all of the tests, but if result is not FAILURE the file will be deleted

        try {
            Thread.sleep(3000);
            this.screenRecorder.stop();

            // Rename newly created file .avi file,
            if (recordName != null && result.getStatus() == ITestResult.FAILURE) {
                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "yyyy-MM-dd HH.mm.ss");
                File newFileName = new File(String.format("%s%s %s.avi",
                        RECORD_DIRECTORY, recordName,
                        dateFormat.format(new Date())));

                this.screenRecorder.getCreatedMovieFiles().get(0)
                        .renameTo(newFileName);
            }

            else {

                this.screenRecorder.getCreatedMovieFiles().get(0)
                        .delete();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }


}
